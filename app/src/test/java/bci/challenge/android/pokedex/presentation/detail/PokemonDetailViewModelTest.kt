package bci.challenge.android.pokedex.presentation.detail

import androidx.lifecycle.Observer
import bci.challenge.android.pokedex.BaseMockitoTest
import bci.challenge.android.pokedex.domain.model.detail.PokemonDetailModel
import bci.challenge.android.pokedex.domain.respository.IPokemonRepository
import bci.challenge.android.pokedex.domain.useCase.GetPokemonDetailUseCase
import bci.challenge.android.pokedex.domain.util.Failure
import bci.challenge.android.pokedex.presentation.ui.detail.PokemonDetailViewModel
import bci.challenge.android.pokedex.presentation.util.Resource
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.*

class PokemonDetailViewModelTest : BaseMockitoTest() {

    private lateinit var pokemonDetailViewModel: PokemonDetailViewModel

    private lateinit var getPokemonDetailUseCase: GetPokemonDetailUseCase

    @Mock
    private lateinit var repository: IPokemonRepository

    @Mock
    private lateinit var observer: Observer<Resource<PokemonDetailModel>>

    @Captor
    private lateinit var pokemonDetailCapture: ArgumentCaptor<Resource<PokemonDetailModel>>

    @Before
    fun setUp() {

        MockitoAnnotations.initMocks(this)

        getPokemonDetailUseCase = Mockito.spy(GetPokemonDetailUseCase(repository))
        pokemonDetailViewModel = PokemonDetailViewModel(getPokemonDetailUseCase)
    }

    private fun getPokemonDetailResponseOK(): PokemonDetailModel {
        return PokemonDetailModel(25, 1, "", null, "Pikachu", null, 200)
    }

    @Test
    fun getPokemonDetail_OK() = runBlocking {

        Mockito.`when`(repository.getPokemonDetail(Mockito.anyString()))
            .thenReturn(getPokemonDetailResponseOK())

        pokemonDetailViewModel.pokemonDetailLiveData.observeForever(observer)

        pokemonDetailViewModel.getPokemonDetail(Mockito.anyString())

        delay(300)

        Mockito.verify(observer, Mockito.times(2))
            .onChanged(pokemonDetailCapture.capture())

        with(pokemonDetailCapture.value) {
            assert(this is Resource.Success)
            assertEquals(getPokemonDetailResponseOK(), this.data)
        }
    }

    @Test
    fun getPokemonDetail_NO_OK() = runBlocking {
        Mockito.`when`(repository.getPokemonDetail(Mockito.anyString()))
            .thenReturn(null)

        pokemonDetailViewModel.pokemonDetailLiveData.observeForever(observer)

        pokemonDetailViewModel.getPokemonDetail(Mockito.anyString())

        delay(300)

        Mockito.verify(observer, Mockito.times(2))
            .onChanged(pokemonDetailCapture.capture())

        with(pokemonDetailCapture.value) {
            assert(this is Resource.Error)
            assert(this.failure is Failure.Error)
        }
    }
}