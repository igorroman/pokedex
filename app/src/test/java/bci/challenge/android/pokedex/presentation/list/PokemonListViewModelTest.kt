package bci.challenge.android.pokedex.presentation.list

import androidx.lifecycle.Observer
import bci.challenge.android.pokedex.BaseMockitoTest
import bci.challenge.android.pokedex.domain.model.NameUrl
import bci.challenge.android.pokedex.domain.model.list.PokemonListModel
import bci.challenge.android.pokedex.domain.respository.IPokemonRepository
import bci.challenge.android.pokedex.domain.useCase.GetPokemonListUseCase
import bci.challenge.android.pokedex.domain.util.Failure
import bci.challenge.android.pokedex.presentation.ui.list.PokemonListViewModel
import bci.challenge.android.pokedex.presentation.util.Resource
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.*

class PokemonListViewModelTest : BaseMockitoTest() {

    private lateinit var pokemonListViewModel: PokemonListViewModel

    private lateinit var getPokemonListUseCase: GetPokemonListUseCase

    @Mock
    private lateinit var repository: IPokemonRepository

    @Mock
    private lateinit var observer: Observer<Resource<PokemonListModel>>

    @Captor
    private lateinit var pokemonListCapture: ArgumentCaptor<Resource<PokemonListModel>>

    @Before
    fun setUp() {

        MockitoAnnotations.initMocks(this)

        getPokemonListUseCase = Mockito.spy(GetPokemonListUseCase(repository))
        pokemonListViewModel = PokemonListViewModel(getPokemonListUseCase)
    }

    private fun getPokemonListResponseOK(): PokemonListModel {
        return PokemonListModel(arrayListOf(NameUrl("Ivysaur", "xxxxxx")))
    }

    @Test
    fun `getPokemonList_OK`() = runBlocking {

        Mockito.`when`(repository.getPokemonList())
            .thenReturn(getPokemonListResponseOK())

        pokemonListViewModel.pokemonListLiveData.observeForever(observer)
        pokemonListViewModel.getPokemonList()

        delay(300)

        Mockito.verify(
            observer,
            Mockito.times(2)
        ).onChanged(
            pokemonListCapture.capture()
        )

        with(pokemonListCapture.value) {
            assert(this is Resource.Success)
            assertEquals(getPokemonListResponseOK(), this.data)
        }
    }

    @Test
    fun getPokemonList_NO_OK() = runBlocking {

        Mockito.`when`(getPokemonListUseCase.doOnBackground(Mockito.anyString()))
            .thenThrow(RuntimeException())

        pokemonListViewModel.pokemonListLiveData.observeForever(observer)

        pokemonListViewModel.getPokemonList()

        delay(300)

        Mockito.verify(
            observer,
            Mockito.times(2)
        ).onChanged(
            pokemonListCapture.capture()
        )

        with(pokemonListCapture.value) {
            assert(this is Resource.Error)
            assert(this.failure is Failure.Error)
        }
    }
}