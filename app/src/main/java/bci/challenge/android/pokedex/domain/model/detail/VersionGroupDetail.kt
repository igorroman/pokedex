package bci.challenge.android.pokedex.domain.model.detail


import bci.challenge.android.pokedex.domain.model.NameUrl
import com.google.gson.annotations.SerializedName

data class VersionGroupDetail(
    @SerializedName("level_learned_at")
    val levelLearnedAt: Int?,
    @SerializedName("move_learn_method")
    val moveLearnMethod: NameUrl?,
    @SerializedName("version_group")
    val versionGroup: NameUrl?
)