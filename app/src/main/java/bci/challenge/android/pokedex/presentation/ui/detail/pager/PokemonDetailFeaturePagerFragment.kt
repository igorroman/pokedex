package bci.challenge.android.pokedex.presentation.ui.detail.pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import bci.challenge.android.pokedex.databinding.FragmentPokemonDetailFeaturesViewPagerBinding

class PokemonDetailFeaturePagerFragment : Fragment() {

    private lateinit var binding: FragmentPokemonDetailFeaturesViewPagerBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPokemonDetailFeaturesViewPagerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.also {

            with(binding.pokemonDetailFeaturesRecycler) {
                addItemDecoration(
                    DividerItemDecoration(
                        requireContext(),
                        DividerItemDecoration.VERTICAL
                    )
                )
                addItemDecoration(
                    DividerItemDecoration(
                        requireContext(),
                        DividerItemDecoration.HORIZONTAL
                    )
                )
                adapter =
                    PokemonDetailFeatureItemAdapter().apply {
                        items =
                            it.getParcelableArrayList<PokemonDetailFeatureItem>("itemList")
                                ?.toList()
                                ?: listOf()
                    }
            }
        }
    }

    companion object {
        fun create(itemList: ArrayList<PokemonDetailFeatureItem>) =
            PokemonDetailFeaturePagerFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList("itemList", itemList)
                }
            }
    }
}