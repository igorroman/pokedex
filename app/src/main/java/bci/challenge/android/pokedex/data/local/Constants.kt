package bci.challenge.android.pokedex.data.local

object Constants {
    const val DATABASE_VERSION = 1
    const val DATABASE_NAME = "bci.pokedex"
    const val DATABASE_ASSETS = "databases/$DATABASE_NAME.db"
}