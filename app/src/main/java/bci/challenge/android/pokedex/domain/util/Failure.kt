package bci.challenge.android.pokedex.domain.util


sealed class Failure {
    data class Error(val description: String) : Failure()
    object NetworkConnection : Failure()
}

