package bci.challenge.android.pokedex.presentation.util.progress

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window
import android.widget.ProgressBar
import bci.challenge.android.pokedex.R

class CustomProgressDialog {
    private var mDialog: Dialog? = null

    fun show(context: Context) {
        mDialog = Dialog(context, R.style.AppCompatAlertDialogStyle)
        mDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        mDialog?.setContentView(R.layout.custom_progress)
        val mProgressBar: ProgressBar = mDialog!!.findViewById(R.id.progress_bar)
        mProgressBar.visibility = View.VISIBLE
        mProgressBar.isIndeterminate = true
        mDialog?.setCancelable(false)
        mDialog?.setCanceledOnTouchOutside(false)
        mDialog?.show()
    }

    fun hideProgress() {
        mDialog?.dismiss()
    }

    fun isShowing(): Boolean = mDialog?.isShowing ?: false

    companion object {
        private var customProgressDialog: CustomProgressDialog? = null
        val instance: CustomProgressDialog?
            get() {
                if (customProgressDialog == null) {
                    customProgressDialog = CustomProgressDialog()
                }
                return customProgressDialog
            }
    }
}