package bci.challenge.android.pokedex.presentation.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import bci.challenge.android.pokedex.R
import bci.challenge.android.pokedex.databinding.AdapterPokemonListItemBinding
import com.bumptech.glide.Glide
import javax.inject.Inject
import bci.challenge.android.pokedex.domain.model.NameUrl as PokemonItem

class PokemonListItemAdapter @Inject constructor() :
    RecyclerView.Adapter<PokemonListItemAdapter.ViewHolder>(), Filterable {

    var items: List<PokemonItem> = listOf()
        set(value) {
            auxItems.addAll(value)
            field = value
        }
    var actionListener: ActionListener? = null

    private var auxItems: ArrayList<PokemonItem> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AdapterPokemonListItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = auxItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val pokemon = auxItems[position]

        val context = holder.itemView.context

        val pokeNumber = pokemon.getId()

        holder.pokemonItemTitle.text = context.getString(
            R.string.item_label_with_number,
            pokemon.name,
            pokeNumber
        )
        holder.itemView.setOnClickListener {
            actionListener?.onGoToDetail(pokemon)
        }
        Glide
            .with(context)
            .load(context.getString(R.string.item_image_url_download, pokeNumber))
            .placeholder(R.drawable.ic_pokemon_logo)
            .centerInside()
            .into(holder.pokemonItemImage)
    }

    class ViewHolder(binding: AdapterPokemonListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val pokemonItemTitle = binding.pokemonEmptyTitle
        val pokemonItemImage = binding.pokemonItemImage
        val pokemonItemGoDetail = binding.pokemonItemGoDetail
    }

    interface ActionListener {
        fun onGoToDetail(item: PokemonItem)
        fun onEmptyFilter(isEmpty: Boolean)
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            return FilterResults().apply {
                values = if (constraint.isNullOrEmpty()) {
                    items as ArrayList
                } else {
                    items.filter { it.name.contains(constraint.trim(), true) } as ArrayList
                }
            }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            auxItems.clear()
            auxItems.addAll(results?.values as ArrayList<PokemonItem>)
            actionListener?.onEmptyFilter(auxItems.isEmpty())
            notifyDataSetChanged()
        }
    }
}