package bci.challenge.android.pokedex.presentation.ui.list

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import bci.challenge.android.pokedex.R
import bci.challenge.android.pokedex.databinding.FragmentPokemonListBinding
import bci.challenge.android.pokedex.domain.model.NameUrl
import bci.challenge.android.pokedex.domain.model.list.PokemonListModel
import bci.challenge.android.pokedex.domain.util.Failure
import bci.challenge.android.pokedex.presentation.appComponent
import bci.challenge.android.pokedex.presentation.util.BaseFragment
import bci.challenge.android.pokedex.presentation.util.extension.gone
import bci.challenge.android.pokedex.presentation.util.extension.hideKeyboard
import bci.challenge.android.pokedex.presentation.util.extension.setNoArrowUpToolbar
import bci.challenge.android.pokedex.presentation.util.extension.visible
import bci.challenge.android.pokedex.presentation.util.handleValue
import bci.challenge.android.pokedex.presentation.util.hasNetworkAvailable
import bci.challenge.android.pokedex.presentation.util.subscribe
import javax.inject.Inject

class PokemonListFragment : BaseFragment<FragmentPokemonListBinding>() {

    override fun layoutId(): Int = R.layout.fragment_pokemon_list

    @Inject
    lateinit var pokemonListItemAdapter: PokemonListItemAdapter

    private val pokemonListViewModel: PokemonListViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        appComponent().inject(this@PokemonListFragment)
        initObserver()
    }

    private fun initObserver() {
        pokemonListViewModel.pokemonListLiveData
            .subscribe(
                this@PokemonListFragment,
                ::showProgress,
                ::handlePokemonListSuccess,
                ::manageFailure
            )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentPokemonListBinding.bind(view)

        with(pokemonListViewModel) {
            pokemonListLiveData
                .handleValue(
                    ::handlePokemonListSuccess,
                    ::manageFailure,
                    ::getPokemonList
                )
        }

        with(binding.pokemonListToolbar.toolbar) {
            title = getString(R.string.pokemon_list)
            setNoArrowUpToolbar(requireActivity())
        }

        binding.pokemonListSwipe.setOnRefreshListener {
            pokemonListViewModel.getPokemonList()
        }
    }

    private fun manageFailure(failure: Failure) {
        showError()
        hideProgress()
    }

    private fun handlePokemonListSuccess(data: PokemonListModel) {

        if (data.items?.isNotEmpty() == true) {
            binding.pokemonListRecycler.adapter = pokemonListItemAdapter.apply {
                items = data.items
                actionListener = object : PokemonListItemAdapter.ActionListener {
                    override fun onGoToDetail(item: NameUrl) {
                        findNavController().navigate(R.id.pokemonDetailFragment, Bundle().apply {
                            putInt("id", item.getId())
                        })
                    }

                    override fun onEmptyFilter(isEmpty: Boolean) {
                        if (isEmpty) {
                            binding.pokemonListEmptyRecycler.visible()
                        } else {
                            binding.pokemonListEmptyRecycler.gone()
                        }
                    }
                }
            }
        } else {
            binding.pokemonListEmptyRecycler.visible()
        }

        hideProgress()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.seach_menu, menu)
        val menuItem = menu.findItem(R.id.action_search)
        val searchView = menuItem.actionView as SearchView
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        searchView.queryHint = getString(R.string.search_title)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                requireActivity().hideKeyboard()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                pokemonListItemAdapter.filter.filter(newText)
                return true
            }
        })
    }

    override fun hideProgress() {
        super.hideProgress()
        binding.pokemonListSwipe.isRefreshing = false
    }

    private fun showError(message: String = getString(R.string.generic_error)) {
        binding.pokemonListToolbar.gone()
        binding.pokemonListError.visible()
        binding.pokemonListError.errorTitle.text = message
        binding.pokemonListError.errorButtonTryAgain.setOnClickListener {

            with(requireActivity()) {
                if (!hasNetworkAvailable(this)) {
                    Toast.makeText(this, getString(R.string.still_offline), Toast.LENGTH_LONG)
                        .show()
                    return@setOnClickListener
                }
            }

            binding.pokemonListToolbar.visible()
            binding.pokemonListError.gone()
            pokemonListViewModel.getPokemonList()
        }
        binding.pokemonListError.errorButtonExit.setOnClickListener {
            requireActivity().finish()
        }
    }
}