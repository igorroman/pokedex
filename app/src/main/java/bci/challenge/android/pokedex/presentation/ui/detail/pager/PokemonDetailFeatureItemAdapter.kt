package bci.challenge.android.pokedex.presentation.ui.detail.pager

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import bci.challenge.android.pokedex.databinding.AdapterPokemonDetailFeatureItemBinding
import bci.challenge.android.pokedex.presentation.util.extension.gone
import bci.challenge.android.pokedex.presentation.util.extension.visible
import javax.inject.Inject

class PokemonDetailFeatureItemAdapter @Inject constructor() :
    RecyclerView.Adapter<PokemonDetailFeatureItemAdapter.ViewHolder>() {

    var items: List<PokemonDetailFeatureItem> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AdapterPokemonDetailFeatureItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = items[position]

        item.label.also {
            holder.pokemonItemLabel.gone()
        }?.also {
            holder.pokemonItemLabel.visible()
            holder.pokemonItemLabel.text = item.label
        }
        holder.pokemonItemValue.text = item.value
    }

    class ViewHolder(binding: AdapterPokemonDetailFeatureItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val pokemonItemLabel = binding.pokemonItemLabel
        val pokemonItemValue = binding.pokemonItemValue
    }
}