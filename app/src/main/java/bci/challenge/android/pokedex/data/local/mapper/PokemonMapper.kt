package bci.challenge.android.pokedex.data.local.mapper

import bci.challenge.android.pokedex.data.local.entity.PokemonEntity
import bci.challenge.android.pokedex.domain.model.NameUrl
import bci.challenge.android.pokedex.domain.model.list.PokemonListModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PokemonMapper @Inject constructor() {

    fun PokemonListModel.toEntity(): List<PokemonEntity> {
        return this.items?.map {
            PokemonEntity(name = it.name, url = it.url)
        } ?: listOf()
    }

    fun List<PokemonEntity>.toModel(): PokemonListModel {
        return PokemonListModel(this.map {
            NameUrl(name = it.name, url = it.url)
        })
    }
}