package bci.challenge.android.pokedex.presentation.util

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerFragmentAdapter(fragment: Fragment) :
    FragmentStateAdapter(fragment) {

    var items: List<Fragment> = mutableListOf()

    override fun getItemCount(): Int = items.size

    override fun createFragment(position: Int): Fragment = items[position]

    fun getItem(position: Int): Fragment = items[position]
}