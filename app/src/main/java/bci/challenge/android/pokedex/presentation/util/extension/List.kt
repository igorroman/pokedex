package bci.challenge.android.pokedex.presentation.util.extension

fun <T> List<T>?.toArrayList(): ArrayList<T> {
    return ArrayList(this ?: listOf())
}