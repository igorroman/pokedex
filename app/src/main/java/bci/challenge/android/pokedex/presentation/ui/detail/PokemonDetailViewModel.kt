package bci.challenge.android.pokedex.presentation.ui.detail

import androidx.lifecycle.MutableLiveData
import bci.challenge.android.pokedex.domain.model.detail.PokemonDetailModel
import bci.challenge.android.pokedex.domain.useCase.GetPokemonDetailUseCase
import bci.challenge.android.pokedex.domain.util.Failure
import bci.challenge.android.pokedex.presentation.util.BaseViewModel
import bci.challenge.android.pokedex.presentation.util.Resource
import javax.inject.Inject

class PokemonDetailViewModel @Inject constructor(private val getPokemonDetailUseCase: GetPokemonDetailUseCase) :
    BaseViewModel() {

    var pokemonDetailLiveData: MutableLiveData<Resource<PokemonDetailModel>> = MutableLiveData()
        private set

    fun getPokemonDetail(id: String) {

        pokemonDetailLiveData.postLoading()

        fun onSuccess(data: PokemonDetailModel) {
            pokemonDetailLiveData.postSuccess(data)
        }

        fun onFailure(failure: Failure) {
            pokemonDetailLiveData.postFailure(failure)
        }

        getPokemonDetailUseCase.execute(
            id,
            ::onSuccess,
            ::onFailure
        )
    }

    override fun onCleared() {
        super.onCleared()
        getPokemonDetailUseCase.dispose()
    }
}