package bci.challenge.android.pokedex.data.remote

import bci.challenge.android.pokedex.domain.model.detail.PokemonDetailModel
import bci.challenge.android.pokedex.domain.model.list.PokemonListModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonRetrofitService {

    @GET("api/v2/pokemon?limit=151")
    fun getPokemonList(): Call<PokemonListModel>

    @GET("api/v2/pokemon/{id}")
    fun getPokemonList(@Path("id") id: String): Call<PokemonDetailModel>
}