package bci.challenge.android.pokedex.domain.util

import android.accounts.NetworkErrorException
import kotlinx.coroutines.*

abstract class BaseUseCase<in Request, out Response> {

    private var job = Job()
    private var uiScope = CoroutineScope(Dispatchers.Main + job)

    @Throws(Exception::class)
    abstract suspend fun doOnBackground(params: Request): Response

    open fun execute(
        params: Request,
        onResult: (Response) -> Unit,
        onFailure: (Failure) -> Unit
    ) {
        uiScope.launch {

            try {
                val result = withContext(Dispatchers.IO) {
                    doOnBackground(params)
                }
                onResult(result)
            } catch (e: Exception) {
                onFailure(
                    when (e) {
                        is NetworkErrorException -> Failure.NetworkConnection
                        else -> Failure.Error(e.message ?: e.toString())
                    }

                )
            }
        }
    }

    open fun dispose() {
        job.cancel()
    }
}