package bci.challenge.android.pokedex.domain.model.list


import bci.challenge.android.pokedex.domain.model.NameUrl
import com.google.gson.annotations.SerializedName

data class PokemonListModel(

    @SerializedName("results")
    val items: List<NameUrl>?
)