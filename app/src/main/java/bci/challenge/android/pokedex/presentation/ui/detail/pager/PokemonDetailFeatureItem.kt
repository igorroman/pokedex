package bci.challenge.android.pokedex.presentation.ui.detail.pager

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PokemonDetailFeatureItem(
    val label: String?,
    val value: String
) : Parcelable