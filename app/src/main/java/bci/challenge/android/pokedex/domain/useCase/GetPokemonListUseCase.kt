package bci.challenge.android.pokedex.domain.useCase

import bci.challenge.android.pokedex.domain.model.list.PokemonListModel
import bci.challenge.android.pokedex.domain.respository.IPokemonRepository
import bci.challenge.android.pokedex.domain.util.BaseUseCase
import javax.inject.Inject

open class GetPokemonListUseCase @Inject constructor(private val repository: IPokemonRepository) :
    BaseGetPokemonListUseCase() {

    override suspend fun doOnBackground(params: Any): PokemonListModel =
        repository.getPokemonList()
}

typealias BaseGetPokemonListUseCase = BaseUseCase<Any, PokemonListModel>