package bci.challenge.android.pokedex.presentation

import android.app.Application
import bci.challenge.android.pokedex.di.component.ApplicationComponent
import bci.challenge.android.pokedex.di.component.DaggerApplicationComponent
import bci.challenge.android.pokedex.presentation.PokedexApplication.Companion.applicationComponent

class PokedexApplication : Application() {

    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        applicationComponent =
            DaggerApplicationComponent
                .builder()
                .context(this)
                .build()
        applicationComponent.inject(this)
    }
}

fun appComponent() = applicationComponent
