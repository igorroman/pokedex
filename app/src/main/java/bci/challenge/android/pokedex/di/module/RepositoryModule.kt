package bci.challenge.android.pokedex.di.module

import bci.challenge.android.pokedex.data.repository.PokemonRepository
import bci.challenge.android.pokedex.domain.respository.IPokemonRepository
import dagger.Binds
import dagger.Module


@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindPokemonRepository(repository: PokemonRepository): IPokemonRepository
}