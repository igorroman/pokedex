package bci.challenge.android.pokedex.di.component

import android.content.Context
import bci.challenge.android.pokedex.di.module.DatabaseModule
import bci.challenge.android.pokedex.di.module.RepositoryModule
import bci.challenge.android.pokedex.di.module.RetrofitModule
import bci.challenge.android.pokedex.di.module.ViewModelModule
import bci.challenge.android.pokedex.presentation.PokedexApplication
import bci.challenge.android.pokedex.presentation.ui.detail.PokemonDetailFragment
import bci.challenge.android.pokedex.presentation.ui.list.PokemonListFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        RepositoryModule::class,
        RetrofitModule::class,
        ViewModelModule::class,
        DatabaseModule::class]
)
interface ApplicationComponent : FragmentInjector {
    fun inject(application: PokedexApplication)

    @Component.Builder
    interface Builder {

        fun build(): ApplicationComponent

        @BindsInstance
        fun context(context: Context): Builder
    }
}

interface FragmentInjector {
    fun inject(application: PokemonListFragment)
    fun inject(pokemonDetailFragment: PokemonDetailFragment)
}
