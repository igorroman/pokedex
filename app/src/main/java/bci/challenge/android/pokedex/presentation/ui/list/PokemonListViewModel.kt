package bci.challenge.android.pokedex.presentation.ui.list

import androidx.lifecycle.MutableLiveData
import bci.challenge.android.pokedex.domain.model.list.PokemonListModel
import bci.challenge.android.pokedex.domain.useCase.GetPokemonListUseCase
import bci.challenge.android.pokedex.domain.util.Failure
import bci.challenge.android.pokedex.presentation.util.BaseViewModel
import bci.challenge.android.pokedex.presentation.util.Resource
import javax.inject.Inject

class PokemonListViewModel @Inject constructor(private val getPokemonListUseCase: GetPokemonListUseCase) :
    BaseViewModel() {

    var pokemonListLiveData: MutableLiveData<Resource<PokemonListModel>> = MutableLiveData()
        private set

    fun getPokemonList() {

        pokemonListLiveData.postLoading()

        fun onSuccess(data: PokemonListModel) {
            pokemonListLiveData.postSuccess(data)
        }

        fun onFailure(failure: Failure) {
            pokemonListLiveData.postFailure(failure)
        }

        getPokemonListUseCase.execute(
            "",
            ::onSuccess,
            ::onFailure
        )
    }

    override fun onCleared() {
        super.onCleared()
        getPokemonListUseCase.dispose()
    }
}