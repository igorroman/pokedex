@file:Suppress("MemberVisibilityCanBePrivate")

package bci.challenge.android.pokedex.presentation.util

import android.os.Bundle
import android.view.*
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import bci.challenge.android.pokedex.presentation.util.progress.CustomProgressDialog
import javax.inject.Inject

abstract class BaseFragment<T : ViewBinding> : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @LayoutRes
    abstract fun layoutId(): Int

    protected lateinit var binding: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId(), container, false)
    }

    /**
     * Deshabilita la vista y el botón back del dispositivo.
     */
    open fun showProgress() {
        activity?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
        disableBackButton()
        if (CustomProgressDialog.instance?.isShowing() == false) {
            CustomProgressDialog.instance?.show(requireContext())
        }
    }

    /**
     * Habilita la vista y el botón back del dispositivo.
     */
    open fun hideProgress() {
        enableBackButton()
        activity?.window?.clearFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
        CustomProgressDialog.instance?.hideProgress()
    }

    protected fun enableBackButton() {
        view?.isFocusableInTouchMode = false
        view?.setOnKeyListener(null)
    }

    protected fun disableBackButton() {
        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener { _, keyCode, _ ->
            keyCode == KeyEvent.KEYCODE_BACK
        }
    }
}