package bci.challenge.android.pokedex.data.repository

import android.accounts.NetworkErrorException
import android.content.Context
import bci.challenge.android.pokedex.data.local.PokemonDatabase
import bci.challenge.android.pokedex.data.local.mapper.PokemonMapper
import bci.challenge.android.pokedex.data.remote.PokemonRetrofitService
import bci.challenge.android.pokedex.domain.model.detail.PokemonDetailModel
import bci.challenge.android.pokedex.domain.model.list.PokemonListModel
import bci.challenge.android.pokedex.domain.respository.IPokemonRepository
import bci.challenge.android.pokedex.presentation.util.hasNetworkAvailable
import javax.inject.Inject

class PokemonRepository @Inject constructor(
    private val service: PokemonRetrofitService,
    private val database: PokemonDatabase,
    private val mapper: PokemonMapper,
    private val context: Context
) : IPokemonRepository {

    @Throws(Exception::class)
    override suspend fun getPokemonList(): PokemonListModel {
        return if (!hasNetworkAvailable(context)) {
            with(mapper) {
                database.pokemonDao.getAll().toModel()
            }
        } else {
            val pokemonList = service.getPokemonList().execute().body()!!
            with(mapper) {
                pokemonList.toEntity().map {
                    database.pokemonDao.insert(it)
                }
            }
            pokemonList
        }
    }

    @Throws(Exception::class)
    override suspend fun getPokemonDetail(id: String): PokemonDetailModel {
        if (!hasNetworkAvailable(context)) throw NetworkErrorException()
        return service.getPokemonList(id).execute().body()!!
    }
}