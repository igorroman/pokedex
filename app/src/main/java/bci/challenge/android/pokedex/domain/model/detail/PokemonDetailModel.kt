package bci.challenge.android.pokedex.domain.model.detail


import com.google.gson.annotations.SerializedName

data class PokemonDetailModel(

    @SerializedName("height")
    val height: Int?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("location_area_encounters")
    val locationAreaEncounters: String?,
    @SerializedName("moves")
    val moves: List<Move>?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("types")
    val types: List<Type>?,
    @SerializedName("weight")
    val weight: Int?
)