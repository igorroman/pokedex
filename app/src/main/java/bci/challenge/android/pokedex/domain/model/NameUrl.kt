package bci.challenge.android.pokedex.domain.model

import android.net.Uri
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NameUrl(

    @SerializedName("name")
    val name: String,
    @SerializedName("url")
    val url: String?
) : Parcelable {
    fun getId(): Int {
        return Uri.parse(url).lastPathSegment?.toInt() ?: 0
    }
}