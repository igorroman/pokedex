package bci.challenge.android.pokedex.domain.model.detail


import bci.challenge.android.pokedex.domain.model.NameUrl
import com.google.gson.annotations.SerializedName

data class Type(
    @SerializedName("slot")
    val slot: Int?,
    @SerializedName("type")
    val type: NameUrl?
)