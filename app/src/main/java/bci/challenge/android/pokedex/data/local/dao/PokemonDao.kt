package bci.challenge.android.pokedex.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.RawQuery
import androidx.sqlite.db.SimpleSQLiteQuery
import bci.challenge.android.pokedex.data.local.entity.PokemonEntity

@Dao
interface PokemonDao {

    @Insert(onConflict = REPLACE)
    fun insert(pokemonList: PokemonEntity)

    @Query("SELECT * FROM pokemon")
    fun getAll(): List<PokemonEntity>

    @Query("DELETE FROM pokemon")
    fun deleteAll()
}