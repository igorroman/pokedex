package bci.challenge.android.pokedex.domain.model.detail


import bci.challenge.android.pokedex.domain.model.NameUrl
import com.google.gson.annotations.SerializedName

data class Move(
    @SerializedName("move")
    val move: NameUrl?,
    @SerializedName("version_group_details")
    val versionGroupDetails: List<VersionGroupDetail>?
)