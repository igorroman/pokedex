package bci.challenge.android.pokedex.domain.model.detail


import bci.challenge.android.pokedex.domain.model.NameUrl
import com.google.gson.annotations.SerializedName

data class Ability(
    @SerializedName("ability")
    val ability: NameUrl?,
    @SerializedName("is_hidden")
    val isHidden: Boolean?,
    @SerializedName("slot")
    val slot: Int?
)