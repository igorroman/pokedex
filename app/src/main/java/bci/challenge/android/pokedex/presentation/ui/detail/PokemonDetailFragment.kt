package bci.challenge.android.pokedex.presentation.ui.detail

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import bci.challenge.android.pokedex.R
import bci.challenge.android.pokedex.databinding.FragmentPokemonDetailBinding
import bci.challenge.android.pokedex.domain.model.detail.PokemonDetailModel
import bci.challenge.android.pokedex.domain.util.Failure
import bci.challenge.android.pokedex.presentation.appComponent
import bci.challenge.android.pokedex.presentation.ui.detail.pager.PokemonDetailFeatureItem
import bci.challenge.android.pokedex.presentation.ui.detail.pager.PokemonDetailFeaturePagerFragment
import bci.challenge.android.pokedex.presentation.util.BaseFragment
import bci.challenge.android.pokedex.presentation.util.PagerFragmentAdapter
import bci.challenge.android.pokedex.presentation.util.extension.gone
import bci.challenge.android.pokedex.presentation.util.extension.setArrowUpToolbar
import bci.challenge.android.pokedex.presentation.util.extension.visible
import bci.challenge.android.pokedex.presentation.util.hasNetworkAvailable
import bci.challenge.android.pokedex.presentation.util.subscribe
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator

class PokemonDetailFragment : BaseFragment<FragmentPokemonDetailBinding>() {

    override fun layoutId(): Int = R.layout.fragment_pokemon_detail

    private val pokemonDetailViewModel: PokemonDetailViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        appComponent().inject(this@PokemonDetailFragment)
        initObserver()
    }

    private fun initObserver() {
        pokemonDetailViewModel.pokemonDetailLiveData.subscribe(
            this@PokemonDetailFragment,
            ::showProgress,
            ::handlePokemonDetailSuccess,
            ::manageFailure
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentPokemonDetailBinding.bind(view)

        with(binding.pokemonDetailToolbar.toolbar) {
            setArrowUpToolbar(requireActivity())
        }
        arguments?.also {

            val pokemonId = it.getInt("id", 0)

            Glide
                .with(requireActivity())
                .load(getString(R.string.item_image_url_download, pokemonId))
                .placeholder(R.drawable.ic_pokemon_logo)
                .centerInside()
                .into(binding.pokemonDetailImage)

            with(pokemonDetailViewModel) {
                pokemonDetailLiveData.value?.also {
                    it.data?.also { detailModel ->
                        handlePokemonDetailSuccess(detailModel)
                    }
                }.also {
                    getPokemonDetail(pokemonId.toString())
                }
            }
        }
    }

    private fun manageFailure(failure: Failure) {

        val message = when (failure) {
            Failure.NetworkConnection -> {
                getString(R.string.network_error)

            }
            else -> {
                getString(R.string.generic_error)
            }
        }
        showError(message)
        hideProgress()
    }

    @Suppress("UNCHECKED_CAST")
    private fun handlePokemonDetailSuccess(data: PokemonDetailModel) {

        with(binding.pokemonDetailToolbar.toolbar) {
            title = data.name?.capitalize()
        }

        val adapter = PagerFragmentAdapter(this@PokemonDetailFragment).apply {
            items =
                mutableListOf(
                    PokemonDetailFeaturePagerFragment.create(
                        data.types?.map {
                            PokemonDetailFeatureItem(
                                null,
                                it.type?.name!!
                            )
                        } as ArrayList<PokemonDetailFeatureItem>
                    ),
                    PokemonDetailFeaturePagerFragment.create(
                        with(data) {
                            arrayListOf(
                                PokemonDetailFeatureItem(
                                    getString(R.string.height),
                                    height.toString()
                                ),
                                PokemonDetailFeatureItem(
                                    getString(R.string.weight),
                                    weight.toString()
                                )
                            )
                        }
                    ), PokemonDetailFeaturePagerFragment.create(
                        data.moves?.map {
                            PokemonDetailFeatureItem(
                                null,
                                it.move?.name!!
                            )
                        } as ArrayList<PokemonDetailFeatureItem>
                    )
                )
        }

        with(binding.pokemonDetailVp) {

            this.adapter = adapter

            registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    val item = adapter.getItem(position)
                    item.view?.post {
                        val wMeasureSpec =
                            View.MeasureSpec.makeMeasureSpec(
                                view?.width!!,
                                View.MeasureSpec.EXACTLY
                            )
                        val hMeasureSpec =
                            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                        item.view?.measure(wMeasureSpec, hMeasureSpec)

                        if (layoutParams.height != view?.measuredHeight) {
                            layoutParams =
                                (layoutParams as ViewGroup.LayoutParams)
                                    .also { lp -> lp.height = item.requireView().measuredHeight }
                        }
                    }
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels)

                }
            })

        }

        attachTabLayout()

        hideError()
        hideProgress()
    }

    private fun attachTabLayout() {
        with(binding.pokemonDetailTab) {
            TabLayoutMediator(
                this,
                binding.pokemonDetailVp
            ) { tab, position ->
                tab.text = when (position) {
                    0 -> getString(R.string.type)
                    1 -> getString(R.string.height)
                    2 -> getString(R.string.moves)
                    else -> getString(R.string.location)
                }
            }.attach()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                requireActivity().onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showError(message: String = getString(R.string.generic_error)) {
        binding.pokemonDetailToolbar.gone()
        binding.pokemonListError.visible()
        binding.pokemonListError.errorTitle.text = message
        binding.pokemonListError.errorButtonTryAgain.setOnClickListener {

            with(requireActivity()) {
                if (!hasNetworkAvailable(this)) {
                    Toast.makeText(this, getString(R.string.still_offline), Toast.LENGTH_LONG)
                        .show()
                    return@setOnClickListener
                }
            }

            hideError()
            arguments?.also {
                pokemonDetailViewModel.getPokemonDetail(it.getInt("id", 0).toString())
            }

        }
        binding.pokemonListError.errorButtonExit.text = getString(R.string.back)
        binding.pokemonListError.errorButtonExit.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun hideError() {
        binding.pokemonDetailToolbar.visible()
        binding.pokemonListError.gone()
    }
}