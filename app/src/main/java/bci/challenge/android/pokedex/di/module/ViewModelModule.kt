package bci.challenge.android.pokedex.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import bci.challenge.android.pokedex.presentation.ui.detail.PokemonDetailViewModel
import bci.challenge.android.pokedex.presentation.ui.list.PokemonListViewModel
import bci.challenge.android.pokedex.presentation.util.ViewModelFactory
import bci.challenge.android.pokedex.presentation.util.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @ViewModelKey(PokemonListViewModel::class)
    @IntoMap
    abstract fun bindPokemonListViewModel(viewModel: PokemonListViewModel): ViewModel

    @Binds
    @ViewModelKey(PokemonDetailViewModel::class)
    @IntoMap
    abstract fun bindPokemonDetailViewModel(viewModel: PokemonDetailViewModel): ViewModel
}