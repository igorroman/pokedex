package bci.challenge.android.pokedex.domain.respository

import bci.challenge.android.pokedex.domain.model.detail.PokemonDetailModel
import bci.challenge.android.pokedex.domain.model.list.PokemonListModel

interface IPokemonRepository {

    @Throws(Exception::class)
    suspend fun getPokemonList(): PokemonListModel

    @Throws(Exception::class)
    suspend fun getPokemonDetail(id : String): PokemonDetailModel
}