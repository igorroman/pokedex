package bci.challenge.android.pokedex.domain.useCase

import bci.challenge.android.pokedex.domain.model.detail.PokemonDetailModel
import bci.challenge.android.pokedex.domain.respository.IPokemonRepository
import bci.challenge.android.pokedex.domain.util.BaseUseCase
import javax.inject.Inject

open class GetPokemonDetailUseCase @Inject constructor(private val repository: IPokemonRepository) :
    BaseGetPokemonDetailUseCase() {

    override suspend fun doOnBackground(params: String): PokemonDetailModel =
        repository.getPokemonDetail(params)
}

typealias BaseGetPokemonDetailUseCase = BaseUseCase<String, PokemonDetailModel>