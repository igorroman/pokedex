package bci.challenge.android.pokedex.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import bci.challenge.android.pokedex.data.local.Constants.DATABASE_VERSION
import bci.challenge.android.pokedex.data.local.dao.PokemonDao
import bci.challenge.android.pokedex.data.local.entity.PokemonEntity

@Database(
    entities = [PokemonEntity::class],
    version = DATABASE_VERSION
)
abstract class PokemonDatabase : RoomDatabase() {
    abstract val pokemonDao: PokemonDao
}