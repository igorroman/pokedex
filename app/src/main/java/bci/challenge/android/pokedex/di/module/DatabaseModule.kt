package bci.challenge.android.pokedex.di.module

import android.content.Context
import androidx.room.Room
import bci.challenge.android.pokedex.data.local.Constants.DATABASE_NAME
import bci.challenge.android.pokedex.data.local.PokemonDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context): PokemonDatabase {
        return Room.databaseBuilder(
            context,
            PokemonDatabase::class.java,
            DATABASE_NAME
        ).allowMainThreadQueries().build()
    }
}